import {databaseConfig, DatabaseConfig} from "./db-config";
import {supersecretConfig, SuperSecretConfig} from './super-secrets';

class Configs {
  private _databaseConfig: DatabaseConfig;
  private _superSecretConfig: SuperSecretConfig;


  constructor() {
    this._databaseConfig = databaseConfig;
    this._superSecretConfig = supersecretConfig;
  }

  getDatabaseConfig(): DatabaseConfig {
    return this._databaseConfig;
  }

  getSuperSecretConfig(): SuperSecretConfig {
    return this._superSecretConfig;
  }
}

export const configs = new Configs();