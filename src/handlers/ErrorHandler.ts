export class ErrorHandler{

    handleError(error: Error, code = 409) {
        if (code != 404) console.log(error);
        let err = {error: true, name: "Error", "message": "Error", "timestamp": new Date().getTime(), code: code};
        if(this.isSequelizeError(error.name)){
            err = {
                error: true,
                name: this.getSequelizeErrors()[error.name].name,
                message: this.getSequelizeErrors()[error.name].message,
                timestamp: new Date().getTime(),
                code: code
            };
        }else{
            err = {
                error: true,
                name: error.name,
                message: error.message,
                timestamp: new Date().getTime(),
                code: code
            };
        }

        return err;
    }

    private isSequelizeError(name: string){
        var re = new RegExp("^[Ss]equelize");
        return re.test(name)?true:false;
    }

    private getSequelizeErrors(){
        return {
            SequelizeUniqueConstraintError: {
                name: "Unique Constraint Error",
                message: "Some of the given values already exist in the database"
            },
            SequelizeAccessDeniedError: {
                name: "Access Denied Error",
                message: "You don't have access to make changes"
            },
            SequelizeConnectionRefusedError: {
                name: "Connection Refused Error",
                message: "Connection refused by the database server"
            },
            SequelizeConnectionTimedOutError: {
                name: "Connection Time out",
                message: "The connection to the database server has timed out"
            },
            SequelizeExclusionConstraintError: {
                name: "Exclusion Constraint Error",
                message: "An exclusion constraint is violated in the database"
            },
            SequelizeForeignKeyConstraintError: {
                name: "Foreign Key Constraint Error",
                message: "A foreign key constraint is violated in the database"
            },
            SequelizeHostNotFoundError: {
                name: "Host not Fount Error",
                message: "A connection to the database has a hostname that was not found"
            },
            SequelizeHostNotReachableError: {
                name: "Host not Reachable Error",
                message: "A connection to the database has a hostname that was not reachable"
            },
            SequelizeInvalidConnectionError: {
                name: "Invalid Connection Error",
                message: "A connection to the database has invalid values for any of the connection parameters"
            },
            SequelizeTimeoutError: {
                name: "Timeout Error",
                message: "A database query times out because of a deadlock"
            },
            SequelizeUnknownConstraintError: {
                name: "Unknown Constraint Error",
                message: "Constraint name is not found in the database"
            },
            SequelizeAssociationError: {
                name: "Association Error",
                message: "An association is improperly constructed (see message for details)"
            },
            SequelizeBulkRecordError: {
                name: "Bulk Record Error",
                message: "Bulk operation fails, it represent per record level error. Used with Promise.AggregateError"
            },
            SequelizeConnectionError: {
                name: "Connection Error",
                message: "There was a connection error"
            },
            SequelizeDatabaseError: {
                name: "Database Error",
                message: "There was a problem connecting to the database"
            },
            SequelizeEagerLoadingError: {
                name: "Eager Loading Error",
                message: "An include statement is improperly constructed"
            },
            SequelizeEmptyResultError: {
                name: "Empty Result Error",
                message: "A record was not found, Usually used with rejectOnEmpty mode"
            },
            SequelizeInstanceError: {
                name: "Instance Error",
                message: "Some problem occurred with Instance methods "
            },
            SequelizeOptimisticLockError: {
                name: "Optimistic Lock Error",
                message: "Attempting to update a stale model instance"
            },
            SequelizeQueryError: {
                name: "Query Error",
                message: "A query is passed invalid options "
            },
            SequelizeScopeError: {
                name: "Scope Error",
                message: "The ORM library(Sequelize) cannot query the specified scope"
            },
            SequelizeValidationError: {
                name: "Validation Error",
                message: "ORM validation has failed"
            }
        };
    }
}