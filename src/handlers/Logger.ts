export class Logger {
    debug: boolean = true;
    name: string;
    constructor(name: string) {
      this.name = name;
    }
    i(message:string) {
      this.log(message,'Info', this.name);
    }
    trace(message: string) {
      this.log(message,'Trace', this.name);
    }
    w(message: string) {
      this.log(message,'Warning', this.name);
    }
    d(message: string) {
      this.log(message,'Debug', this.name);
    }
    e(message: string, exception: object) {
      this.log(`${message} Exception(${JSON.stringify(exception)})`,'Exception', this.name);
    }

    log(message: string, level:string, instance: string){
      if(this.debug){
        let date = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
        switch(level){
          case 'i':
            console.log(`[${date}]: ${level.toUpperCase()}@(${instance}) -> \x1b[32m%s\x1b[0m`, message);
            break;
          case 'e':
            console.log(`[${date}]: ${level.toUpperCase()}@(${instance}) -> \x1b[35m%s\x1b[0m`, message);
            break;
          case 'trace':
            console.log(`[${date}]: ${level.toUpperCase()}@(${instance}) -> \x1b[35m%s\x1b[0m`, message);
            break;
          case 'd':
            console.log(`[${date}]: ${level.toUpperCase()}@(${instance}) -> \x1b[34m%s\x1b[0m`, message);
            break;
          case 'e':
            console.log(`[${date}]: ${level.toUpperCase()}@(${instance}) -> \x1b[37m%s\x1b[0m`, message);
            break;
          case 'w':
            console.log(`[${date}]: ${level.toUpperCase()}@(${instance}) -> \x1b[37m%s\x1b[0m`, message);
            break;
        }
      }
    }
  }
  
  export class LoggerFactory{
    getLogger(name: string):Logger{
      return new Logger(name);
    }
  }
  
  
  /* 
  Reset = "\x1b[0m"
  Bright = "\x1b[1m"
  Dim = "\x1b[2m"
  Underscore = "\x1b[4m"
  Blink = "\x1b[5m"
  Reverse = "\x1b[7m"
  Hidden = "\x1b[8m"
  
  FgBlack = "\x1b[30m"
  FgRed = "\x1b[31m"
  FgGreen = "\x1b[32m"
  FgYellow = "\x1b[33m"
  FgBlue = "\x1b[34m"
  FgMagenta = "\x1b[35m"
  FgCyan = "\x1b[36m"
  FgWhite = "\x1b[37m"
  
  BgBlack = "\x1b[40m"
  BgRed = "\x1b[41m"
  BgGreen = "\x1b[42m"
  BgYellow = "\x1b[43m"
  BgBlue = "\x1b[44m"
  BgMagenta = "\x1b[45m"
  BgCyan = "\x1b[46m"
  BgWhite = "\x1b[47m"
  */