import { models, sequelize, SequelizeModels } from "../models/index";
import { Sequelize } from "sequelize";

export class Picker {

    static model(): { model: SequelizeModels, sequelize: Sequelize } {
        return { model: models, sequelize: sequelize };
    }
}
