import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import { AppCredentialsAttributes, AppCredentialsInstance } from "./interfaces/app-credentials";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<AppCredentialsInstance, AppCredentialsAttributes> {
    let table = sequelize.define<AppCredentialsInstance, AppCredentialsAttributes>(
      'AppCredentials', {
        id: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        app: {
            type: dataTypes.STRING(45),
            allowNull: false
        },
        privateKey: {
            type: dataTypes.STRING,
            allowNull: false
        },
        publicKey: {
            type: dataTypes.STRING(32),
            allowNull: false
        },
        userId: {
            type: dataTypes.INTEGER(11),
            allowNull: false
        }
    },
    {
        timestamps: true,
        paranoid: true,
        tableName: 'qbi_app_credentials'
    });
    return table;
}