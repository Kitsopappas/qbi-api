import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {AuthenticationProviderAttributes, AuthenticationProviderInstance} from "./interfaces/authentication-provider";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<AuthenticationProviderInstance, AuthenticationProviderAttributes> {
    let table = sequelize.define<AuthenticationProviderInstance, AuthenticationProviderAttributes>(
      'AuthenticationProvider', {
        providerKey: {
            type: dataTypes.STRING(128),
            primaryKey: true,
            autoIncrement: true
        },
        userId: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            allowNull: false
        },
        providerType: {
            type: dataTypes.INTEGER(1).UNSIGNED,
            primaryKey: true,
            allowNull: false
        }
    },
    {
        timestamps: false,
        paranoid: false,
        tableName: 'qbi_authentication_provider'
    });
    return table;
}

