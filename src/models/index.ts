import * as fs from "fs";
import * as path from "path";
import * as SequelizeStatic from "sequelize";
import {configs} from "../configs/configs";

/* POSTS IMPORTS */
import { Sequelize } from "sequelize";
import { UserAttributes, UserInstance } from "./interfaces/users";
import { UserTypesInstance, UserTypesAttributes } from "./interfaces/user-types";
import { SiteInstance, SiteAttributes } from "./interfaces/site";
import { SiteSectionsInstance, SiteSectionsAttributes } from "./interfaces/site-sections";
import { SiteImagesInstance, SiteImagesAttributes } from "./interfaces/site-images";
import { SiteContactInfoInstance, SiteContactInfoAttributes } from "./interfaces/site-contact-info";
import {AuthenticationProviderInstance, 
        AuthenticationProviderAttributes,
        ProviderTypeInstance,
        ProviderTypeAttributes} from "./interfaces/authentication-provider";
import { IssueReportAttributes, IssueReportInstance } from "./interfaces/issue-report";
import { AppCredentialsAttributes, AppCredentialsInstance } from "./interfaces/app-credentials";

export interface SequelizeModels {
  User: SequelizeStatic.Model<UserInstance, UserAttributes>;
  UserTypes: SequelizeStatic.Model<UserTypesInstance, UserTypesAttributes>;
  Site: SequelizeStatic.Model<SiteInstance, SiteAttributes>;
  SiteSections: SequelizeStatic.Model<SiteSectionsInstance, SiteSectionsAttributes>;
  SiteImages: SequelizeStatic.Model<SiteImagesInstance, SiteImagesAttributes>;
  SiteContactInfo: SequelizeStatic.Model<SiteContactInfoInstance, SiteContactInfoAttributes>;
  AuthenticationProvider: SequelizeStatic.Model<AuthenticationProviderInstance, AuthenticationProviderAttributes>;
  ProviderType: SequelizeStatic.Model<ProviderTypeInstance, ProviderTypeAttributes>;
  IssueReport: SequelizeStatic.Model<IssueReportInstance, IssueReportAttributes>;
  AppCredentials: SequelizeStatic.Model<AppCredentialsInstance, AppCredentialsAttributes>;
}

class Database {

  private _basename: string;
  private _models: SequelizeModels;
  private _sequelize: Sequelize;

  constructor() {
    this._basename = path.basename(module.filename);
    let dbConfig = configs.getDatabaseConfig();

    this._sequelize = new SequelizeStatic(dbConfig.database, dbConfig.username,
      dbConfig.password, dbConfig);
    this._models = ({} as any);

    fs.readdirSync(__dirname).filter((file: string) => {
      return (file !== this._basename) && (file !== "interfaces");
    }).forEach((file: string) => {
      let model = this._sequelize.import(path.join(__dirname, file));
      this._models[(model as any).name] = model;
    });

    Object.keys(this._models).forEach((modelName: string) => {
      if (typeof this._models[modelName].associate === "function") {
        this._models[modelName].associate(this._models);
      }
    });

    this.makeRelations();
  }

  getModels() {
    return this._models;
  }

  getSequelize() {
    return this._sequelize;
  }

  makeRelations() {
    this._models.User.belongsTo(this._models.UserTypes, {foreignKey: 'type'});
    // this._models.User.hasOne(this._models.UserTypes, {foreignKey: 'type'});
    this._models.Site.hasMany(this._models.SiteImages, {foreignKey: 'site_id'});
    this._models.Site.hasMany(this._models.SiteSections, {foreignKey: 'site_id'});
    this._models.Site.hasMany(this._models.SiteContactInfo, {foreignKey: 'site_id'});

  }
}

const database = new Database();
export const models = database.getModels();
export const sequelize = database.getSequelize();