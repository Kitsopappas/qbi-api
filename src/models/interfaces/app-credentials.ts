import { Instance } from "sequelize";

export interface AppCredentialsAttributes {
    id?: number;
    app?: string;
    privateKey?: string;
    publicKey?: string;
    userId?: number;
    createdAt?: string;
    updatedAt?: string;
    deletedAt?: string;

}

export interface AppCredentialsInstance extends Instance<AppCredentialsAttributes> {
    dataValues: AppCredentialsAttributes;
}