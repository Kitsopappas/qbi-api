import { Instance } from "sequelize";

export interface AuthenticationProviderAttributes {
    providerKey?: string;
    userId?: number;
    providerType?: ProviderType;
}

export interface AuthenticationProviderInstance extends Instance<AuthenticationProviderAttributes> {
  dataValues: AuthenticationProviderAttributes;
}

export interface ProviderTypeAttributes {
    id: number;
    provider: string;
}

export interface ProviderTypeInstance extends Instance<ProviderTypeAttributes> {
  dataValues: ProviderTypeAttributes;
}