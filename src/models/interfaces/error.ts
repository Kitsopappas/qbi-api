export interface ErrorType {
    error: boolean;
    name: string;
    message: string;
    timestamp: number;
    code: number
}