import { Instance } from "sequelize";

export interface IssueReportAttributes {
    id?: number;
    type?: string;
    userId?: number;
    siteId?: number;
    reporterEmail?: string;
    title?: string;
    message?: string;
    createdAt?: string;
    updatedAt?: string;
    deletedAt?: string;

}

export interface IssueReportInstance extends Instance<IssueReportAttributes> {
    dataValues: IssueReportAttributes;
}