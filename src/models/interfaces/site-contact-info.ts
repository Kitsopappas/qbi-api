import { Instance } from "sequelize";

export interface SiteContactInfoAttributes {
    id?: number;
    site_id?: number;
    type?: string;
    contact?: string;
    language?: number;
}

export interface SiteContactInfoInstance extends Instance<SiteContactInfoAttributes> {
    dataValues: SiteContactInfoAttributes;
}