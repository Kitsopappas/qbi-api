import { Instance } from "sequelize";

export interface SiteImagesAttributes {
    id?: number;
    site_id?: number;
    alt?: string;
    file?: string;
    is_main?: number;
}

export interface SiteImagesInstance extends Instance<SiteImagesAttributes> {
    dataValues: SiteImagesAttributes;
}