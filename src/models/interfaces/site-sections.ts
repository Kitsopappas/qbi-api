import { Instance } from "sequelize";

export interface SiteSectionsAttributes {
    id?: number;
    site_id?: number;
    title?: string;
    content?: string;
    language?: string;
    level?: number;
}

export interface SiteSectionsInstance extends Instance<SiteSectionsAttributes> {
    dataValues: SiteSectionsAttributes;
}