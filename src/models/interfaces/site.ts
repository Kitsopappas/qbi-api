import { Instance } from "sequelize";

export interface SiteAttributes {
    id?: number;
    userId?: number;
    credential?: string;
    latitude?: number;
    longitude?: number;
}

export interface SiteInstance extends Instance<SiteAttributes> {
    dataValues: SiteAttributes;
}