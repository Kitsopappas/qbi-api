import { Instance } from "sequelize";

export interface UserTypesAttributes {
  id?: number;
  type?: string;
}

export interface UserTypesInstance extends Instance<UserTypesAttributes> {
  dataValues: UserTypesAttributes;
}