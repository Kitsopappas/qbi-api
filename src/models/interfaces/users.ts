import { Instance } from "sequelize";

export interface UserAttributes {
  id?: number;
  email?: string;
  password?: string;
  salt?: string;
  iterations?: number;
  type?: number;
}

export interface UserInstance extends Instance<UserAttributes> {
  dataValues: UserAttributes;
}