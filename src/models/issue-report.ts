import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {IssueReportAttributes, IssueReportInstance} from "./interfaces/issue-report";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<IssueReportInstance, IssueReportAttributes> {
    let table = sequelize.define<IssueReportInstance, IssueReportAttributes>(
      'IssueReport', {
        id: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: dataTypes.STRING,
            allowNull: false
        },
        userId: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            allowNull: false
        },
        siteId: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            allowNull: false
        },
        reporterEmail: {
            type: dataTypes.STRING(65),
            allowNull: false
        },
        title: {
            type: dataTypes.STRING(45),
            allowNull: false
        },
        message: {
            type: dataTypes.STRING,
            allowNull: false
        }
    },
    {
        timestamps: true,
        paranoid: false,
        tableName: 'qbi_issue_report'
    });
    return table;
}