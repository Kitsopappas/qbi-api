import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {ProviderTypeAttributes, ProviderTypeInstance} from "./interfaces/authentication-provider";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<ProviderTypeInstance, ProviderTypeAttributes> {
    let table = sequelize.define<ProviderTypeInstance, ProviderTypeAttributes>(
      'ProviderType', {
        id: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        provider: {
            type: dataTypes.STRING(20),
            allowNull: false
        }
    },
    {
        timestamps: false,
        paranoid: false,
        tableName: 'qbi_provider_type'
    });
    return table;
}