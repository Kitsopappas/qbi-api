import * as SequelizeStatic from "sequelize";
import { DataTypes, Sequelize } from "sequelize";
import { SiteContactInfoInstance, SiteContactInfoAttributes } from "./interfaces/site-contact-info";

export default function (sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<SiteContactInfoInstance, SiteContactInfoAttributes> {
    let table = sequelize.define<SiteContactInfoInstance, SiteContactInfoAttributes>(
        'SiteContactInfo', {
            id: {
                type: dataTypes.INTEGER(11).UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },
            site_id: {
                type: dataTypes.INTEGER(11).UNSIGNED,
                references: {
                    model: 'Site',// site migration define
                    key: 'id'
                },
                allowNull: false
            },
            type: {
                type: dataTypes.ENUM('email', 'phone'),
                allowNull: false
            },
            contact: {
                type: dataTypes.STRING(80),
                allowNull: false
            },
            language: {
                type: dataTypes.STRING(8),
                allowNull: false
            }
        },
        {
            timestamps: true,
            paranoid: true,
            tableName: 'qbi_site_contact_info'
        });
    return table;
}