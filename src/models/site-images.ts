import * as SequelizeStatic from "sequelize";
import { DataTypes, Sequelize } from "sequelize";
import { SiteImagesInstance, SiteImagesAttributes } from "./interfaces/site-images";

export default function (sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<SiteImagesInstance, SiteImagesAttributes> {
    let table = sequelize.define<SiteImagesInstance, SiteImagesAttributes>(
        'SiteImages', {
            id: {
                type: dataTypes.INTEGER(11).UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },
            site_id: {
                type: dataTypes.INTEGER(11).UNSIGNED,
                references: {
                    model: 'Site',// site migration define
                    key: 'id'
                },
                allowNull: false
            },
            alt: {
                type: dataTypes.STRING(20),
                allowNull: false
            },
            file: {
                type: dataTypes.STRING(80),
                allowNull: false
            },
            is_main: {
                type: dataTypes.TINYINT(1),
                allowNull: false,
                defaultValue: 0
            }
        },
        {
            timestamps: true,
            paranoid: true,
            tableName: 'qbi_site_images'
        });
    return table;
}