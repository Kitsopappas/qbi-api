import * as SequelizeStatic from "sequelize";
import { DataTypes, Sequelize } from "sequelize";
import { SiteSectionsInstance, SiteSectionsAttributes } from "./interfaces/site-sections";

export default function (sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<SiteSectionsInstance, SiteSectionsAttributes> {
    let table = sequelize.define<SiteSectionsInstance, SiteSectionsAttributes>(
        'SiteSections', {
            id: {
                type: dataTypes.INTEGER(11).UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },
            site_id: {
                type: dataTypes.INTEGER(11).UNSIGNED,
                references: {
                    model: 'Site',// site migration define
                    key: 'id'
                },
                allowNull: false
            },
            title: {
                type: dataTypes.STRING(50),
                allowNull: false
            },
            content: {
                type: dataTypes.STRING,
                allowNull: false
            },
            language: {
                type: dataTypes.STRING(8),
                allowNull: false
            },
            level: {
                type: dataTypes.INTEGER(3),
                allowNull: false
            }
        },
        {
            timestamps: true,
            paranoid: true,
            tableName: 'qbi_site_sections'
        });
    return table;
}