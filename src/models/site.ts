import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import { SiteInstance, SiteAttributes } from "./interfaces/site";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<SiteInstance, SiteAttributes> {
    let table = sequelize.define<SiteInstance, SiteAttributes>(
      'Site', {
        id: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        userId: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            allowNull: false
        },
        credential: {
            type: dataTypes.STRING(50),
            allowNull: false
        },
        latitude: {
            type: dataTypes.DOUBLE,
            allowNull: false
        },
        longitude: {
            type: dataTypes.DOUBLE,
            allowNull: false
        }
    },
    {
        timestamps: true,
        paranoid: true,
        tableName: 'qbi_site'
    });
    return table;
}