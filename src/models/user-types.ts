import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {UserTypesAttributes, UserTypesInstance} from "./interfaces/user-types";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<UserTypesInstance, UserTypesAttributes> {
    let table = sequelize.define<UserTypesInstance, UserTypesAttributes>(
      'UserTypes', {
        id: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: dataTypes.STRING(30),
            allowNull: false
        }
    },
    {
        timestamps: false,
        paranoid: false,
        tableName: 'qbi_user_types'
    });
    return table;
}