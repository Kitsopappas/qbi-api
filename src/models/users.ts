import * as SequelizeStatic from "sequelize";
import {DataTypes, Sequelize} from "sequelize";
import {UserAttributes, UserInstance} from "./interfaces/users";

export default function(sequelize: Sequelize, dataTypes: DataTypes):
    SequelizeStatic.Model<UserInstance, UserAttributes> {
    let table = sequelize.define<UserInstance, UserAttributes>(
      'User', {
        id: {
            type: dataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: dataTypes.STRING,
            allowNull: false
        },
        password: {
            type: dataTypes.STRING,
            allowNull: false
        },
        salt: {
            type: dataTypes.STRING,
            allowNull: false
        },
        iterations: {
            type: dataTypes.INTEGER(1),
            allowNull: false
        },
        type: {
            type: dataTypes.INTEGER(1),
            allowNull: false
        }
    },
    {
        timestamps: true,
        paranoid: true,
        tableName: 'qbi_user'
    });
    return table;
}