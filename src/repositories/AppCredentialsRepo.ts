import { SequelizeModels } from "../models";
import { Picker } from "../handlers/Picker";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { LoggerFactory } from "../handlers/Logger";
import { AppCredentialsAttributes, AppCredentialsInstance } from "../models/interfaces/app-credentials";

export class AppCredentialsRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("AppCredentialsRepo");

    create(attributes: AppCredentialsAttributes) {
        return new Promise<AppCredentialsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().AppCredentials;
            model.create(attributes).then((instance: AppCredentialsInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    update(attributes: AppCredentialsAttributes) {
        return new Promise<AppCredentialsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().AppCredentials;
            model.findOne({ where: { id: attributes.id } }).then((instance: AppCredentialsInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error, 409));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "User not found", message: "Could not find any user with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: AppCredentialsAttributes, attributes = null) {
        return new Promise<AppCredentialsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().AppCredentials;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findOne(query).then((instance: AppCredentialsInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Issue Report not found", message: "Could not find any issue report with the provided data" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export const appCredentialsRepo: AppCredentialsRepo = new AppCredentialsRepo();