import { SequelizeModels } from "../models";
import { Picker } from "../handlers/Picker";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { LoggerFactory } from "../handlers/Logger";
import { IssueReportAttributes, IssueReportInstance } from "../models/interfaces/issue-report";

export class IssueReportRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("IssueReportRepo");

    /**
     * @param attributes The user attributes to create a new user as seen at {@link UserAttributes}
     * @description Create a new user
     */
    create(attributes: IssueReportAttributes) {
        return new Promise<IssueReportInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().IssueReport;
            model.create(attributes).then((instance: IssueReportInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The user attributes to create a new user as seen at {@link UserAttributes}
     * @description Update the user if exists
     */
    update(attributes: IssueReportAttributes) {
        return new Promise<IssueReportInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().IssueReport;
            model.findOne({ where: { id: attributes.id } }).then((instance: IssueReportInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "User not found", message: "Could not find any user with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: IssueReportAttributes, attributes = null) {
        return new Promise<IssueReportInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().IssueReport;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            query['include'] = [{
                model: this.getModels().UserTypes,
                attributes: ['type']
            }];
            model.findOne(query).then((instance: IssueReportInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Issue Report not found", message: "Could not find any issue report with the provided data" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: IssueReportAttributes, attributes = null) {
        return new Promise<Array<IssueReportInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().IssueReport;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            query['include'] = [{
                model: this.getModels().UserTypes,
                attributes: ['type']
            }];
            model.findAll(query).then((instances: Array<IssueReportInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export const issueReportRepo: IssueReportRepo = new IssueReportRepo();