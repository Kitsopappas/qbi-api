import { SequelizeModels } from "../models";
import { Transaction } from 'sequelize';
import { Picker } from "../handlers/Picker";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { LoggerFactory } from "../handlers/Logger";
import { SiteAttributes, SiteInstance } from "../models/interfaces/site";
import { SiteSectionsAttributes, SiteSectionsInstance } from "../models/interfaces/site-sections";
import { SiteContactInfoAttributes, SiteContactInfoInstance } from "../models/interfaces/site-contact-info";
import { SiteImagesAttributes, SiteImagesInstance } from "../models/interfaces/site-images";

export class SiteRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("siteRepo");

    /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteAttributes}
     * @description Create a new site
     */
    create(attributes: SiteAttributes) {
        return new Promise<SiteInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            model.create(attributes).then((instance: SiteInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The site attributes to update a site as seen at {@link SiteAttributes}
     * @description Update the site if exists
     */
    update(attributes: SiteAttributes) {
        return new Promise<SiteInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            model.findOne({ where: { id: attributes.id } }).then((instance: SiteInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find any site with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: SiteAttributes, attributes = null) {
        return new Promise<SiteInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }
            model.findOne(query).then((instance: SiteInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find any site with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: SiteAttributes, attributes = null) {
        return new Promise<Array<SiteInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findAll(query).then((instances: Array<SiteInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAllMultipleTables(where: SiteAttributes, attributes = null, include = null) {
        return new Promise<Array<SiteInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            if (include != null) {
                query["include"] = include;
            }
            model.findAll(query).then((instances: Array<SiteInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOneMultipleTables(where: SiteAttributes, attributes = null, include = null) {
        return new Promise<SiteInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            if (include != null) {
                query["include"] = include;
            }
            model.findOne(query).then((instance: SiteInstance) => {
                resolve(instance);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findNSitesNearLocation(geolocation: { latitude: number, longitude: number, accuracy: number }, include = null) {
        return new Promise<Array<SiteInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().Site;
            const sequelize = this.getSequelize();
            const distance = sequelize.fn('getDistance', geolocation.latitude, geolocation.longitude, sequelize.col('latitude'), sequelize.col('longitude'));

            let query: any = {
                order: distance,
                where: sequelize.where(distance, { $lte: geolocation.accuracy }),
            };

            if (include != null) {
                query["include"] = include;
            }
            model.findAll(query).then((res) => {
                resolve(res);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export class SiteSectionsRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("SiteSectionsRepo");

    /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteSectionsAttributes}
     * @description Create a new site
     */
    create(attributes: SiteSectionsAttributes) {
        return new Promise<SiteSectionsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteSections;
            model.create(attributes).then((instance: SiteSectionsInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteSectionsAttributes}
     * @description Create or update site sections
     */
    bulkCreateOrUpdate(attributes: Array<SiteSectionsAttributes>)
    {
        return new Promise<SiteSectionsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteSections;
            model.bulkCreate(attributes, {updateOnDuplicate: ['title', 'content', 'language']}).then((instance: Array<SiteSectionsInstance>) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The site attributes to update a site as seen at {@link SiteSectionsAttributes}
     * @description Update the site if exists
     */
    update(attributes: SiteAttributes) {
        return new Promise<SiteSectionsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteSections;
            model.findOne({ where: { id: attributes.id } }).then((instance: SiteSectionsInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find any site with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: SiteSectionsAttributes, attributes = null) {
        return new Promise<SiteSectionsInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteSections;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }
            model.findOne(query).then((instance: SiteSectionsInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Site section not found", message: "Could not find any site sections with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: SiteSectionsAttributes, attributes = null) {
        return new Promise<Array<SiteSectionsInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteSections;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findAll(query).then((instances: Array<SiteSectionsInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    delete(id: number)
    {
        return new Promise((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteSections;
            model.destroy({where: {
                id: id
            }}).then((res)=> {
                resolve(res);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export class SiteContactInfoRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("SiteContactInfoRepo");

    /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteContactInfoAttributes}
     * @description Create a new site
     */
    create(attributes: SiteContactInfoAttributes) {
        return new Promise<SiteContactInfoInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteContactInfo;
            model.create(attributes).then((instance: SiteContactInfoInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteSectionsAttributes}
     * @description Create or update site contact info
     */
    bulkCreateOrUpdate(attributes: Array<SiteContactInfoAttributes>)
    {
        return new Promise<SiteContactInfoInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteContactInfo;
            model.bulkCreate(attributes, {updateOnDuplicate: ['type', 'contact', 'language']}).then((instance: Array<SiteContactInfoInstance>) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The site attributes to update a site as seen at {@link SiteContactInfoAttributes}
     * @description Update the site if exists
     */
    update(attributes: SiteContactInfoAttributes, site_id: number, language: string) {
        return new Promise<SiteContactInfoInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteContactInfo;
            model.findOne({ where: { site_id: site_id, language: language } }).then((instance: SiteContactInfoInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "Site's contact info not found", message: "Could not find any contact info with the provided site id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: SiteContactInfoAttributes, attributes = null) {
        return new Promise<SiteContactInfoInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteContactInfo;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }
            model.findOne(query).then((instance: SiteContactInfoInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Site's contact info not found", message: "Could not find any contact info with the provided site id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: SiteContactInfoAttributes, attributes = null) {
        return new Promise<Array<SiteContactInfoInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteContactInfo;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findAll(query).then((instances: Array<SiteContactInfoInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    delete(id: number)
    {
        return new Promise((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteContactInfo;
            model.destroy({where: {
                id: id
            }}).then((res)=> {
                resolve(res);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export class SiteImagesRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("SiteSectionsRepo");

    /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteImagesAttributes}
     * @description Create a new site
     */
    create(attributes: SiteImagesAttributes) {
        return new Promise<SiteImagesInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteImages;
            model.create(attributes).then((instance: SiteImagesInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

     /**
     * @param attributes The site attributes to create a new site as seen at {@link SiteSectionsAttributes}
     * @description Create or update site contact info
     */
    bulkCreateOrUpdate(attributes: Array<SiteImagesAttributes>)
    {
        return new Promise<SiteImagesInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteImages;
            model.bulkCreate(attributes, {updateOnDuplicate: ['file', 'alt']}).then((instance: Array<SiteImagesInstance>) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The site attributes to update a site as seen at {@link SiteImagesAttributes}
     * @description Update the site if exists
     */
    update(attributes: SiteImagesAttributes) {
        return new Promise<SiteImagesInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteImages;
            model.findOne({ where: { site_id: attributes.site_id, id: attributes.id } }).then((instance: SiteImagesInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find any site with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: SiteImagesAttributes, attributes = null) {
        return new Promise<SiteImagesInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteImages;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }
            model.findOne(query).then((instance: SiteImagesInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Site's image info not found", message: "Could not find any image with the provided site id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: SiteImagesAttributes, attributes = null) {
        return new Promise<Array<SiteImagesInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteImages;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findAll(query).then((instances: Array<SiteImagesInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    delete(id: number)
    {
        return new Promise((resolve: Function, reject: Function) => {
            const model = this.getModels().SiteImages;
            model.destroy({where: {
                id: id
            }}).then((res)=> {
                resolve(res);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export const siteRepo: SiteRepo = new SiteRepo();
export const siteSectionsRepo: SiteSectionsRepo = new SiteSectionsRepo();
export const siteContactInfoRepo: SiteContactInfoRepo = new SiteContactInfoRepo();
export const siteImagesRepo: SiteImagesRepo = new SiteImagesRepo();