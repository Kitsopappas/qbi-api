import { SequelizeModels } from "../models";
import { UserAttributes, UserInstance } from "../models/interfaces/users";
import { Transaction } from 'sequelize';
import { Picker } from "../handlers/Picker";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { LoggerFactory } from "../handlers/Logger";

export class UserRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("UserRepo");

    /**
     * @param attributes The user attributes to create a new user as seen at {@link UserAttributes}
     * @description Create a new user
     */
    create(attributes: UserAttributes) {
        return new Promise<UserInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().User;
            model.create(attributes).then((instance: UserInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    /**
     * @param attributes The user attributes to create a new user as seen at {@link UserAttributes}
     * @description Update the user if exists
     */
    update(attributes: UserAttributes) {
        return new Promise<UserInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().User;
            model.findOne({ where: { id: attributes.id } }).then((instance: UserInstance) => {
                if (instance) {
                    instance.update(attributes).then((u) => {
                        resolve(u);
                    }).catch((error: Error) => {
                        reject(this.errorHandler.handleError(error));
                    });
                } else {
                    reject(this.errorHandler.handleError({ name: "User not found", message: "Could not find any user with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findOne(where: UserAttributes, attributes = null) {
        return new Promise<UserInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().User;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            query['include'] = [{
                model: this.getModels().UserTypes,
                attributes: ['type']
            }];
            model.findOne(query).then((instance: UserInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    resolve(null);
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: UserAttributes, attributes = null) {
        return new Promise<UserInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().User;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            query['include'] = [{
                model: this.getModels().UserTypes,
                attributes: ['type']
            }];
            model.findAll(query).then((instances: Array<UserInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export const userRepo: UserRepo = new UserRepo();