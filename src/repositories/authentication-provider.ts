import { SequelizeModels } from "../models";
import { Transaction } from 'sequelize';
import { Picker } from "../handlers/Picker";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { LoggerFactory } from "../handlers/Logger";
import {AuthenticationProviderInstance, 
    AuthenticationProviderAttributes,
    ProviderTypeInstance,
    ProviderTypeAttributes} from "../models/interfaces/authentication-provider";

export class AuthenticationProviderRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("siteRepo");

    create(attributes: AuthenticationProviderAttributes) {
        return new Promise<AuthenticationProviderInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().AuthenticationProvider;
            model.create(attributes).then((instance: AuthenticationProviderInstance) => {
                resolve(instance);
            }).catch((error: Error) => {
                reject(this.errorHandler.handleError(error));
            });
        });
    }

    findOne(where: AuthenticationProviderAttributes, attributes = null) {
        return new Promise<AuthenticationProviderInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().AuthenticationProvider;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }
            model.findOne(query).then((instance: AuthenticationProviderInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Authentication Provider User not found", message: "Could not find any auth provider with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: AuthenticationProviderAttributes, attributes = null) {
        return new Promise<Array<AuthenticationProviderInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().AuthenticationProvider;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findAll(query).then((instances: Array<AuthenticationProviderInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export class ProviderTypeRepo {
    errorHandler: ErrorHandler = new ErrorHandler();
    logger = new LoggerFactory().getLogger("siteRepo");

    findOne(where: ProviderTypeAttributes, attributes = null) {
        return new Promise<ProviderTypeInstance>((resolve: Function, reject: Function) => {
            const model = this.getModels().ProviderType;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }
            model.findOne(query).then((instance: ProviderTypeInstance) => {
                if (instance) {
                    resolve(instance);
                } else {
                    reject(this.errorHandler.handleError({ name: "Provider type not found", message: "Could not find any auth provider with the provided id" }, 404));
                }
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    findAll(where: ProviderTypeAttributes, attributes = null) {
        return new Promise<Array<ProviderTypeInstance>>((resolve: Function, reject: Function) => {
            const model = this.getModels().ProviderType;
            let query = {};
            if (where != null) {
                query['where'] = where;
            }

            if (attributes != null) {
                query['attributes'] = attributes;
            }

            model.findAll(query).then((instances: Array<ProviderTypeInstance>) => {
                resolve(instances);
            }).catch((error) => {
                reject(this.errorHandler.handleError(error, 409));
            });
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct sequelize instance to execute the transactions
     */
    getSequelize() {
        return Picker.model().sequelize;
    }
}

export const authenticationProviderRepo: AuthenticationProviderRepo = new AuthenticationProviderRepo();
export const providerTypeRepo: ProviderTypeRepo = new ProviderTypeRepo();