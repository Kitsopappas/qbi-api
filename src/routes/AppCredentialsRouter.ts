import { Request, Response, Router, NextFunction } from "express";
import { ErrorType } from '../models/interfaces/error';
import { appCredentialsService } from "../services/AppCredentials";
import { authService } from "../services/Auth";

export const router = Router();
export class AppCredentialsRouter {

    router: Router

    /**
     * Initialize the SiteRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    create(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            req.body["userId"] = decoded["user_id"];
            appCredentialsService.create(req.body).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    update(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            req.body["userId"] = decoded["user_id"];
            appCredentialsService.update(req.body).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }
    
    getById(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            appCredentialsService.getById(decoded["user_id"]).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.create);
        this.router.put('/', this.update);
        this.router.get('/', this.getById);
    }
}

// Create the SiteRouter, and export its configured Express.Router
const appCredentialsRouter = new AppCredentialsRouter();
appCredentialsRouter.init();

export default appCredentialsRouter.router;