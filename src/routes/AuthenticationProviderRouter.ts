import { Request, Response, Router, NextFunction } from "express";
import { ErrorType } from '../models/interfaces/error';
import { authService } from "../services/Auth";
import { authenticationProviderService, providerTypeService } from "../services/AuthenticationProvider";

export const router = Router();
export class AuthenticationProviderRouter {

    router: Router

    /**
     * Initialize the SiteRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    createAuthenticationProvider(req: Request, res: Response, next: NextFunction)
    {
        authenticationProviderService.create(req.body.email, req.body).then((r) => {
            return res.status(200).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    getAvailableProviders(req: Request, res: Response, next: NextFunction) {
        providerTypeService.getAvailableProviders().then((r) => {
            return res.status(200).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    getvailableProvidersForUser(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then(decoded => {
            authenticationProviderService.getAvailableAuthenticationProvidersForUser(decoded["user_id"]).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error) => {
            return res.status(error.code).send(error);
        });
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/providers', this.getAvailableProviders);
        this.router.post('/', this.createAuthenticationProvider);
        this.router.get('/user-available', this.getvailableProvidersForUser);
    }
}

// Create the SiteRouter, and export its configured Express.Router
const authenticationProviderRouter = new AuthenticationProviderRouter();
authenticationProviderRouter.init();

export default authenticationProviderRouter.router;