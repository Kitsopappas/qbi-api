import { Request, Response, Router, NextFunction } from "express";
import { ErrorType } from '../models/interfaces/error';
import { providerTypeService } from "../services/AuthenticationProvider";
import { issueReportService } from "../services/IssueReport";

export const router = Router();
export class IssueReportRouter {

    router: Router

    /**
     * Initialize the SiteRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    create(req: Request, res: Response, next: NextFunction) {
        issueReportService.create(req.body).then((r) => {
            return res.status(201).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    update(req: Request, res: Response, next: NextFunction) {
        issueReportService.update(req.body).then((r) => {
            return res.status(200).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }
    
    getById(req: Request, res: Response, next: NextFunction) {
        issueReportService.getById(+req.params.reportId).then((r) => {
            return res.status(200).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    getAll(req: Request, res: Response, next: NextFunction) {
        issueReportService.getAll().then((r) => {
            return res.status(200).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.create);
        this.router.put('/', this.update);
        this.router.get('/', this.getAll);
        this.router.get('/:reportId', this.getById);
    }
}

// Create the SiteRouter, and export its configured Express.Router
const issueReportRouter = new IssueReportRouter();
issueReportRouter.init();

export default issueReportRouter.router;