import { Request, Response, Router, NextFunction } from "express";
import { ErrorType } from '../models/interfaces/error';
import { authService } from "../services/Auth";
import { siteService } from "../services/Site";

export const router = Router();
export class SitePublicRouter {

    router: Router

    /**
     * Initialize the SiteRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    getAllForUser(req: Request, res: Response, next: NextFunction)
    {
        authService.publicApi(req).then((decoded) => {
            siteService.getAllSitesForUser(decoded["userId"]).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        }); 
    }

    getOne(req: Request, res: Response, next: NextFunction) {
        let id: number = +req.params.id;
        authService.publicApi(req).then((decoded) => {
            siteService.getSite(id, decoded["userId"]).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    getSitesNearLocation(req: Request, res: Response, next: NextFunction)
    {
        var accuracy = req.params.accuracy != null? req.params.accuracy : 100;
        authService.publicApi(req).then((decoded) => {
            siteService.getSitesNearLocation({latitude: +req.params.lat, longitude: +req.params.lng, accuracy: +accuracy}).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.getAllForUser);
        this.router.get('/location/:lat/:lng', this.getSitesNearLocation);
        this.router.get('/location/:lat/:lng/:accuracy', this.getSitesNearLocation);
        this.router.get('/:id', this.getOne);
    }
}

// Create the SiteRouter, and export its configured Express.Router
const sitePublicRouter = new SitePublicRouter();
sitePublicRouter.init();

export default sitePublicRouter.router;