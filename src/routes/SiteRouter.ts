import { Request, Response, Router, NextFunction } from "express";
import { ErrorType } from '../models/interfaces/error';
import { authService } from "../services/Auth";
import { siteService } from "../services/Site";

export const router = Router();
export class SiteRouter {

    router: Router

    /**
     * Initialize the SiteRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    initializeSite(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            req.body["userId"] = decoded["user_id"];
            siteService.initializeSite(req.body).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    create(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            siteService.createSite(decoded["user_id"], req.body).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        }); 
    }

    update(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            siteService.updateSite(decoded["user_id"], req.body).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        }); 
    }

    getAllForUser(req: Request, res: Response, next: NextFunction)
    {
        authService.check(req).then((decoded) => {
            siteService.getAllSitesForUser(decoded["user_id"]).then((r) => {
                return res.status(201).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        }); 
    }

    getOne(req: Request, res: Response, next: NextFunction) {
        let id: number = +req.params.id;
        authService.check(req).then((decoded) => {
            siteService.getSite(id, decoded["user_id"]).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((e: ErrorType) => {
            siteService.getSite(id).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        });
    }

    getSitesNearLocation(req: Request, res: Response, next: NextFunction)
    {
        var accuracy = req.params.accuracy != null? req.params.accuracy : 100;
        siteService.getSitesNearLocation({latitude: +req.params.lat, longitude: +req.params.lng, accuracy: +accuracy}).then((r) => {
            return res.status(200).send(r);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    deleteImage(req: Request, res: Response, next: NextFunction)
    {
        authService.check(req).then((decoded) => {
            siteService.delete(+req.params.id, 'image').then((r) => {
                return res.status(200).send(r);
            }).catch((error) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        }); 
    }

    deleteContact(req: Request, res: Response, next: NextFunction)
    {
        authService.check(req).then((decoded) => {
            siteService.delete(+req.params.id, 'contact').then((r) => {
                return res.status(200).send(r);
            }).catch((error) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    deleteSection(req: Request, res: Response, next: NextFunction)
    {
        authService.check(req).then((decoded) => {
            siteService.delete(+req.params.id, 'section').then((r) => {
                return res.status(200).send(r);
            }).catch((error) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        }); 
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/init', this.initializeSite);
        this.router.post('/', this.create);
        this.router.put('/', this.update);
        this.router.get('/', this.getAllForUser);
        this.router.get('/location/:lat/:lng', this.getSitesNearLocation);
        this.router.get('/location/:lat/:lng/:accuracy', this.getSitesNearLocation);
        this.router.delete('/contact/:id', this.deleteContact);
        this.router.delete('/section/:id', this.deleteSection);
        this.router.delete('/image/:id', this.deleteImage);
        this.router.get('/:id', this.getOne);
    }
}

// Create the SiteRouter, and export its configured Express.Router
const siteRouter = new SiteRouter();
siteRouter.init();

export default siteRouter.router;