import { Request, Response, Router } from "express";
import { loadCollection } from '../handlers/utils';
import * as multer from 'multer';
import * as fs from 'fs';
import * as Loki from 'lokijs';

// setup
const DB_NAME = 'db.json';
const COLLECTION_NAME = 'images';
const UPLOAD_PATH = 'uploads';
const upload = multer({ dest: `${UPLOAD_PATH}/` }); // multer configuration
const db = new Loki(`${UPLOAD_PATH}/${DB_NAME}`, { persistenceMethod: 'fs' });

export const router = Router();
export class UploadRouter {

    router: Router;

    /**
     * Initialize the SiteRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async uploadFile(req: Request, res: Response) {
        try {
            const col = await loadCollection(COLLECTION_NAME, db);
            const data = col.insert(req.file);
    
            db.saveDatabase();
            res.send({ id: data.$loki, fileName: data.filename, originalName: data.originalname });
        } catch (err) {
            res.status(400);
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', upload.single('photo'), this.uploadFile);
    }
}

// Create the SiteRouter, and export its configured Express.Router
const uploadRouter = new UploadRouter();
uploadRouter.init();

export default uploadRouter.router;