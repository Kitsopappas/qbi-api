import { userService } from '../services/User';
import { UserInstance } from "../models/interfaces/users";
import { Request, Response, Router, NextFunction } from "express";
import { authService } from '../services/Auth';
import { ErrorType } from '../models/interfaces/error';

export const router = Router();
export class UserRouter {

    router: Router

    /**
     * Initialize the UserRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    create(req: Request, res: Response, next: NextFunction) {
        userService.create(req.body).then((user: UserInstance) => {
            return res.status(201).send(user);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    update(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            userService.update(decoded["user_id"], req.body).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    login(req: Request, res: Response, next: NextFunction) {
        userService.login(req.body.email, req.body.password).then((user: UserInstance) => {
            return res.status(200).send(user);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    loginWithAuthenticationProvider(req: Request, res: Response, next: NextFunction) {
        userService.loginWithAuthenticationProvider(req.body.providerKey, req.body.providerType).then((user: UserInstance) => {
            return res.status(200).send(user);
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    get(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            userService.getById(decoded["user_id"]).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    getByEmail(req: Request, res: Response, next: NextFunction) {
        authService.check(req).then((decoded) => {
            userService.getByEmail(req.params.email).then((r) => {
                return res.status(200).send(r);
            }).catch((error: ErrorType) => {
                return res.status(error.code).send(error);
            });
        }).catch((error: ErrorType) => {
            return res.status(error.code).send(error);
        });
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.create);
        this.router.put('/', this.update);
        this.router.post('/login', this.login);
        this.router.post('/oauth', this.loginWithAuthenticationProvider);
        this.router.get('/', this.get);
        this.router.get('/:email', this.getByEmail);
    }
}

// Create the UserRouter, and export its configured Express.Router
const userRoutes = new UserRouter();
userRoutes.init();

export default userRoutes.router;