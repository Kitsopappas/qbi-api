import { AppCredentialsAttributes, AppCredentialsInstance } from "../models/interfaces/app-credentials";
import { appCredentialsRepo } from "../repositories/AppCredentialsRepo";
import { configs } from "../configs/configs";
import * as crypto from 'crypto';
import { ErrorHandler } from "../handlers/ErrorHandler";

const IV_LENGTH = 16; // For AES, this is always 16

export class AppCredentialsService {

    create(attributes: AppCredentialsAttributes) {
        attributes["publicKey"] = this.makePublicKey();
        attributes["privateKey"] = this.makePrivateKey(attributes.publicKey);
        console.log(this.decrypt(attributes.privateKey));
        return appCredentialsRepo.create(attributes);
    }

    update(attributes: AppCredentialsAttributes) {
        return appCredentialsRepo.update(attributes);
    }

    getById(userId: number) {
        return appCredentialsRepo.findOne({ userId: userId }, ['app', 'publicKey', 'privateKey', 'createdAt', 'updatedAt', 'deletedAt']);
    }

    authorizeAccess(publicKey: string)
    {
        return new Promise((resolve: Function, reject: Function) => {
            appCredentialsRepo.findOne({publicKey: publicKey}).then((appCred: AppCredentialsInstance) => {
                const decryptedKey = this.decrypt(appCred.dataValues.privateKey);
                if (decryptedKey === publicKey)
                {
                    const user = {
                        userId: appCred.dataValues.userId
                    };
                    resolve(user);
                } else {
                    const error = new Error("Your api key does not have any access");
                    reject(new ErrorHandler().handleError(error, 409));
                }
            }).catch((e) => {
                reject(e);
            });
        });
    }

    private makePrivateKey(text) {
        let iv = crypto.randomBytes(IV_LENGTH);
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(configs.getSuperSecretConfig().tokenSecretPublicApi), iv);
        let encrypted = cipher.update(text);

        encrypted = Buffer.concat([encrypted, cipher.final()]);

        return iv.toString('hex') + ':' + encrypted.toString('hex');
    }

    private makePublicKey(): string {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 32; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    private decrypt(text): string {
        let textParts = text.split(':');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = Buffer.from(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(configs.getSuperSecretConfig().tokenSecretPublicApi), iv);
        let decrypted = decipher.update(encryptedText);

        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }

}

export const appCredentialsService = new AppCredentialsService();