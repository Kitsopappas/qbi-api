import { configs } from "../configs/configs";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { appCredentialsService } from "./AppCredentials";
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

export class AuthService {

    errorHandler: ErrorHandler = new ErrorHandler();

    /**
     * @param req The current request to the api
     * @description Get the current token of the request and decrypt it to get the available data
     */
    check(req) {
        let promise = new Promise((resolve: Function, reject: Function) => {
            let token = req.body.authorization || req.query.authorization || req.headers['authorization']; // decode token
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }
            if (token) {
                jwt.verify(token, configs.getSuperSecretConfig().tokenSecret, (err, decoded) => {
                    if (err) {
                        reject(this.errorHandler.handleError({name: "Authentication Error", message: "Could not find token"}, 401));
                    } else {
                        resolve(decoded);
                    }
                });
            } else {
                reject(this.errorHandler.handleError({name: "Authentication Error", message: "No token provided"}, 401));
            }
        });

        return promise;
    }

    publicApi(req)
    {
        return new Promise((resolve: Function, reject: Function) => {
            let apiKey = req.body['x-api-key'] || req.query['x-api-key'] || req.headers['x-api-key']; // decode token
            if (apiKey)
            {
                appCredentialsService.authorizeAccess(apiKey).then((res: {userId: number}) => {
                    resolve(res);
                }).catch((e)=>{
                    reject(e);
                });
            } else {
                reject(this.errorHandler.handleError({name: "Authentication Error", message: "Please provide an api key"}, 401));
            }
        });
    }

    /**
     * @param v1 Version one to check
     * @param comparator The compare event we want to check (>||=, <||=, ==||=, !=||=)
     * @param v2 Version two to check
     * @description Check the comparison between two versions
     */
    compareVersions(v1, comparator, v2) {
        var comparator = comparator == '=' ? '==' : comparator;
        if(['==','===','<','<=','>','>=','!=','!=='].indexOf(comparator) == -1) {
            throw new Error('Invalid comparator. ' + comparator);
        }
        var v1parts = v1.split('.'), v2parts = v2.split('.');
        var maxLen = Math.max(v1parts.length, v2parts.length);
        var part1, part2;
        var cmp = 0;
        for(var i = 0; i < maxLen && !cmp; i++) {
            part1 = parseInt(v1parts[i], 10) || 0;
            part2 = parseInt(v2parts[i], 10) || 0;
            if(part1 < part2)
                cmp = 1;
            if(part1 > part2)
                cmp = -1;
        }
        return eval('0' + comparator + cmp);
    }
}

export const authService = new AuthService();
