import { ErrorHandler } from "../handlers/ErrorHandler";
import {
    AuthenticationProviderAttributes,
    AuthenticationProviderInstance,
    ProviderTypeInstance,
} from "../models/interfaces/authentication-provider";
import { UserInstance } from "../models/interfaces/users";
import { authenticationProviderRepo, providerTypeRepo } from "../repositories/authentication-provider";
import { userService } from "./User";

export class AuthenticationProviderService {

    create(email: string, attributes: AuthenticationProviderAttributes) {
        return new Promise((resolve: Function, reject: Function) => {
            userService.getByEmailPrivate(email).then((user: UserInstance) => {
                attributes["userId"] = user.dataValues.id;
                authenticationProviderRepo.create(attributes).then((prov: AuthenticationProviderInstance) => {
                    resolve(prov);
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                reject(e);
            }); 
        });
    }

    findOne(attributes: AuthenticationProviderAttributes) {
        return authenticationProviderRepo.findOne(attributes);
    }

    getAvailableAuthenticationProvidersForUser(userId: number)
    {
        return authenticationProviderRepo.findAll({userId: userId});
    }

    getById(email: string, providerType: number) {
        return authenticationProviderRepo.findOne({ providerKey: email, providerType: providerType }, null);
    }
}

export class ProviderTypeService {
    private errorHandler: ErrorHandler = new ErrorHandler();

    getAvailableProviders() {
        return new Promise((resolve: Function, reject: Function) => {
            providerTypeRepo.findAll(null).then((res: Array<ProviderTypeInstance>) => {
                resolve(res);
            }).catch((e: Error) => {
                reject(this.errorHandler.handleError(e, 409));
            });
        });
    }
}

export const authenticationProviderService = new AuthenticationProviderService();
export const providerTypeService = new ProviderTypeService();