import { IssueReportAttributes } from "../models/interfaces/issue-report";
import { SiteInstance } from "../models/interfaces/site";
import { issueReportRepo } from "../repositories/IssueReportRepo";
import { siteService } from "./Site";

export class IssueReportService {

  create(attributes: IssueReportAttributes) {
    return new Promise((resolve: Function, reject: Function) => {
      siteService.getSite(attributes.siteId).then((res: SiteInstance) => {
        const userId = res.dataValues.userId;
        attributes["userId"] = userId;
        issueReportRepo.create(attributes).then((r) => {
          resolve(r);
        }).catch((e) => {
          reject(e);
        });
      }).catch((e) => {
        reject(e);
      });
    });
  }

  update(attributes: IssueReportAttributes) {
    return issueReportRepo.update(attributes);
  }

  getById(reportId: number) {
    return issueReportRepo.findOne({ id: reportId }, null);
  }

  getAll() {
    return issueReportRepo.findAll(null, null);
  }
}

export const issueReportService = new IssueReportService();