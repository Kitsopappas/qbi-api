import { ErrorHandler } from "../handlers/ErrorHandler";
import { SiteAttributes, SiteInstance } from "../models/interfaces/site";
import { siteRepo, siteContactInfoRepo, siteImagesRepo, siteSectionsRepo } from "../repositories/SiteRepo";
import { SiteContactInfoAttributes, SiteContactInfoInstance } from "../models/interfaces/site-contact-info";
import { SiteImagesAttributes, SiteImagesInstance } from "../models/interfaces/site-images";
import { SiteSectionsAttributes, SiteSectionsInstance } from "../models/interfaces/site-sections";
import sequelize = require("sequelize");
import { SequelizeModels } from "../models";
import { Picker } from "../handlers/Picker";

export class SiteService {

    private errorHandler: ErrorHandler = new ErrorHandler();

    initializeSite(attributes: SiteAttributes) {
        return siteRepo.create(attributes);
    }

    createSite(userId: number, attr: { siteAttr: SiteAttributes, contactAttr: Array<SiteContactInfoAttributes>, imagesAttr: Array<SiteImagesAttributes>, sectionsAttr: Array<SiteSectionsAttributes> }) {
        return new Promise((resolve: Function, reject: Function) => {
            siteRepo.getSequelize().transaction((t: sequelize.Transaction) => {
                let promises = [];
                attr.siteAttr.userId = userId;
                return siteRepo.findOne({ id: attr.siteAttr.id, userId: userId }).then((site: SiteInstance) => {
                    if (site) {
                        for (let a of attr.contactAttr) {
                            promises.push(this.createContactInfo(a, attr.siteAttr.id));
                        }

                        for (let i of attr.imagesAttr) {
                            promises.push(this.createImage(i, attr.siteAttr.id));
                        }

                        for (let s of attr.sectionsAttr) {
                            promises.push(this.createSection(s, attr.siteAttr.id));
                        }

                        site.update(attr.siteAttr).then(r => {
                            Promise.all(promises).then(() => {
                                resolve(r);
                            }).catch((e) => {
                                t.rollback().then((roll) => {
                                    reject(e);
                                }).catch((err) => {
                                    reject(e);
                                });
                            });
                        }).catch((e) => {
                            reject(e);
                        });
                    } else {
                        reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find qbi site with the specific id" }, 404));
                    }
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                reject(this.errorHandler.handleError(e));
            });
        });
    }

    updateSite(userId: number, attr: { siteAttr: SiteAttributes, contactAttr: Array<SiteContactInfoAttributes>, imagesAttr: Array<SiteImagesAttributes>, sectionsAttr: Array<SiteSectionsAttributes> })
    {
        return new Promise((resolve: Function, reject: Function) => {
            siteRepo.getSequelize().transaction((t: sequelize.Transaction) => {
                let promises = [];
                return siteRepo.findOne({ id: attr.siteAttr.id, userId: userId}).then((site: SiteInstance) => {
                    if (site) {
                        for (let i = 0; i < attr.contactAttr.length; i++) {
                            attr.contactAttr[i]['site_id'] = attr.siteAttr.id;
                        }

                        for (let i = 0; i < attr.imagesAttr.length; i++) {
                            attr.imagesAttr[i]['site_id'] = attr.siteAttr.id;
                        }

                        for (let i = 0; i < attr.sectionsAttr.length; i++) {
                            attr.sectionsAttr[i]['site_id'] = attr.siteAttr.id;
                        }

                        if (attr.contactAttr.length > 0)
                        {
                            promises.push(siteContactInfoRepo.bulkCreateOrUpdate(attr.contactAttr));
                        }

                        if (attr.imagesAttr.length > 0)
                        {
                            promises.push(siteImagesRepo.bulkCreateOrUpdate(attr.imagesAttr));
                        }

                        if (attr.sectionsAttr.length > 0)
                        {
                            promises.push(siteSectionsRepo.bulkCreateOrUpdate(attr.sectionsAttr));
                        }

                        promises.push(site.update(attr.siteAttr));
                        Promise.all(promises).then((r) => {
                            resolve(r);
                        }).catch((e) => {
                            t.rollback().then((roll) => {
                                reject(roll);
                            }).catch((err) => {
                                reject(e);
                            });
                        });
                    } else {
                        reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find qbi site with the specific id" }, 404));
                    }
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                reject(this.errorHandler.handleError(e));
            });
        });
    }

    getSite(site_id: number, ownUserId: number = -1) {
        return new Promise((resolve: Function, reject: Function) => {
            siteRepo.findOneMultipleTables({ id: site_id }, null, 
                [
                    {
                        model: this.getModels().SiteImages
                    },
                    {
                        model: this.getModels().SiteContactInfo
                    },
                    {
                        model: this.getModels().SiteSections
                    }
                ]).then((site: SiteInstance) => {
                    if (site) {
                        if (ownUserId > 0)
                        {
                            site.dataValues["isMySite"] = ownUserId == site["userId"];
                        }
                        resolve(site);
                    } else {
                        reject(this.errorHandler.handleError({ name: "Site not found", message: "Could not find qbi site with the specific id" }, 404));
                    }
            }).catch((e) => {
                reject(e);
            });
        });
    }

    getSitesNearLocation(geolocation: {latitude: number, longitude: number, accuracy: number}) {
        return new Promise((resolve: Function, reject: Function) => {
            siteRepo.findNSitesNearLocation(geolocation, 
                [
                    {
                        model: this.getModels().SiteImages
                    },
                    {
                        model: this.getModels().SiteContactInfo
                    },
                    {
                        model: this.getModels().SiteSections
                    }
                ]).then((site: Array<SiteInstance>) => {
                    if (site) {
                        resolve(site);
                    } else {
                        reject(this.errorHandler.handleError({ name: "Sites not found", message: "Could not find qbi site with the specific id" }, 404));
                    }
            }).catch((e) => {
                reject(e);
            });
        });
    }

    getAllSitesForUser(user_id: number) {
        return new Promise((resolve: Function, reject: Function) => {
            siteRepo.findAllMultipleTables({userId: user_id}, null, 
                [
                    {
                        model: this.getModels().SiteImages
                    },
                    {
                        model: this.getModels().SiteContactInfo
                    },
                    {
                        model: this.getModels().SiteSections
                    }
                ]).then(res=>{
                    resolve(res);
                }).catch(e=>reject(e));
        });
    }

    createSection(attr: SiteSectionsAttributes, site_id: number) {
        return new Promise((resolve: Function, reject: Function) => {
            attr["site_id"] = site_id;
            siteSectionsRepo.findOne({ id: attr.id }).then((section: SiteSectionsInstance) => {
                section.update(attr).then(r => {
                    resolve(r);
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                if (e.code == 404) {
                    siteSectionsRepo.create(attr).then(r => {
                        resolve(r);
                    }).catch((e) => {
                        reject(e);
                    });
                }
            })
        });
    }

    createImage(attr: SiteImagesAttributes, site_id: number) {
        return new Promise((resolve: Function, reject: Function) => {
            attr["site_id"] = site_id;
            siteImagesRepo.findOne({ id: attr.id }).then((image: SiteImagesInstance) => {
                image.update(attr).then(r => {
                    resolve(r);
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                if (e.code == 404) {
                    siteImagesRepo.create(attr).then(r => {
                        resolve(r);
                    }).catch((e) => {
                        reject(e);
                    });
                }
            })
        });
    }

    createContactInfo(attr: SiteContactInfoAttributes, site_id: number) {
        return new Promise((resolve: Function, reject: Function) => {
            console.log("SiteContactInfoAttributes: ", attr);
            attr["site_id"] = site_id;
            siteContactInfoRepo.findOne({ id: attr.id }).then((contactInfo: SiteContactInfoInstance) => {
                contactInfo.update(attr).then(r => {
                    resolve(r);
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                if (e.code == 404) {
                    siteContactInfoRepo.create(attr).then(r => {
                        resolve(r);
                    }).catch((e) => {
                        reject(e);
                    });
                }
            });
        });
    }

    delete(id: number, type: string)
    {
        return new Promise((resolve: Function, reject: Function) => {
            switch(type)
            {
                case 'section':
                    siteSectionsRepo.delete(id).then((res)=>resolve({deleted: true})).catch((e)=>reject(e));
                    break;
                case 'image':
                    siteImagesRepo.delete(id).then((res)=>resolve({deleted: true})).catch((e)=>reject(e));
                    break;
                case 'contact':
                    siteContactInfoRepo.delete(id).then((res)=>resolve({deleted: true})).catch((e)=>reject(e));
                    break;
                default:
                    reject(new ErrorHandler().handleError(new Error("Could not find any site type to delete"), 409));
            }
        });
    }

    /**
     * @param app The app name to pick the correct database
     * @description Given an application name pick the correct database models to execute the transactions
     */
    getModels(): SequelizeModels {
        return Picker.model().model;
    }
}

export const siteService = new SiteService();