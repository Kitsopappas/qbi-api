import { UserAttributes, UserInstance } from "../models/interfaces/users";
import { configs } from "../configs/configs";
import { ErrorHandler } from "../handlers/ErrorHandler";
import { userRepo } from "../repositories/UserRepo";
import * as crypto from 'crypto';
import { authenticationProviderService } from "./AuthenticationProvider";
import { AuthenticationProviderInstance, ProviderTypeInstance } from "../models/interfaces/authentication-provider";
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

export class UserService {

  private errorHandler: ErrorHandler = new ErrorHandler();

  create(attributes: UserAttributes) {
    return new Promise((resolve: Function, reject: Function) => {
      userRepo.findOne({email: attributes.email}).then((user: UserInstance) => {
        if (user)
        {
          reject(this.errorHandler.handleError({name: "USER_EXISTS", message: "The user you are trying to create already exists"}, 409));
        } else {
          attributes["salt"] = this.makeSalt();
          attributes["iterations"] = this.makeIterations();
          attributes["password"] = this.generateUserPassword(attributes.password, attributes.salt, attributes.iterations); // Generate a hashed password for the user
          userRepo.create(attributes).then((createdUser: UserInstance) => {
            resolve(createdUser);
          }).catch((e) => {
            reject(e);
          });
        }
      });
    });
  }

  login(email: string, password: string) {
    return new Promise((resolve: Function, reject: Function) => {
      userRepo.findOne({ email: email }).then((user: UserInstance) => {
        let payload = {
          user_id: user.dataValues.id,
          timestamp: new Date()
        };
        if (!user) {
          reject(this.errorHandler.handleError({name: "Authentication failed.", message: "User not found in database with the provided email"}, 404));
        } else if (user) {
          if (user.dataValues.password !== this.generateUserPassword(password, user.dataValues.salt, user.dataValues.iterations)) {
            reject(this.errorHandler.handleError({name: "Authentication failed.", message: "Password incorrect"}, 401));
          } else {
            const token = jwt.sign(payload, configs.getSuperSecretConfig().tokenSecret);
            resolve({ success: true, message: 'Authentication success', token: token, time: new Date(), code: 200 });
          }
        }
      }).catch((error) => {
        reject(this.errorHandler.handleError(error));
      });
    });
  }

  loginWithAuthenticationProvider(providerKey: string, providerType: number)
  {
    return new Promise((resolve: Function, reject: Function) => {
      authenticationProviderService.findOne({providerKey: providerKey, providerType: providerType}).then((res: AuthenticationProviderInstance) => {
        const userId = res.dataValues.userId;
        let payload = {
          user_id: userId,
          timestamp: new Date()
        };
        if (res)
        {
          const token = jwt.sign(payload, configs.getSuperSecretConfig().tokenSecret);
          resolve({ success: true, message: 'Authentication success', token: token, time: new Date(), code: 200 });
        } else {
          reject(this.errorHandler.handleError({name: "Authentication failed.", message: "Failed to login with authentication provider"}, 401));
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  update(userId: number, attributes: UserAttributes) {
    attributes["id"] = userId;
    if (attributes.password != null) {
      attributes["salt"] = this.makeSalt();
      attributes["iterations"] = this.makeIterations();
      attributes["password"] = this.generateUserPassword(attributes.password, attributes.salt, attributes.iterations); // Generate a hashed password for the user
    }
    return userRepo.update(attributes);
  }

  getById(userId: number) {
    return userRepo.findOne({ id: userId }, ['email', 'createdAt', 'updatedAt', 'deletedAt']);
  }

  getByEmail(email: string) {
    return userRepo.findOne({ email: email }, ['email', 'createdAt', 'updatedAt', 'deletedAt']);
  }

  getByEmailPrivate(email: string) {
    return userRepo.findOne({ email: email }, ['id', 'email', 'createdAt', 'updatedAt', 'deletedAt']);
  }

  getAll() {
    return userRepo.findAll(null, ['email', 'createdAt', 'updatedAt', 'deletedAt']);
  }

  /* Helper functions */
  generateUserPassword(pass, salt, iterations) {
    return crypto.pbkdf2Sync(pass, salt, iterations, 32, 'sha512').toString('hex');
  }

  makeSalt() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 9; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  makeIterations() {
    return Math.floor(Math.random() * 9) + 1;
  }

  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

  /**
   * Converts array buffer to a hex value
   * @param {buffer} buffer The array buffer we want to convert to a hex value
   *
   * @return {hex} Returns a hex value in a string format
   */
  buf2hex(buffer) { // buffer is an ArrayBuffer
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}

export const userService = new UserService();