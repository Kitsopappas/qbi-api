-- --------------------------------------------------------
-- Διακομιστής:                  127.0.0.1
-- Έκδοση διακομιστή:            10.1.31-MariaDB - mariadb.org binary distribution
-- Λειτ. σύστημα διακομιστή:     Win32
-- HeidiSQL Έκδοση:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for qbi
CREATE DATABASE IF NOT EXISTS `qbi` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `qbi`;

-- Dumping structure for πίνακας qbi.qbi_site
CREATE TABLE IF NOT EXISTS `qbi_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credential` varchar(50) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for πίνακας qbi.qbi_site_contact_info
CREATE TABLE IF NOT EXISTS `qbi_site_contact_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) NOT NULL,
  `type` enum('email','phone','fax','messenger','instagram','facebook') NOT NULL,
  `contact` varchar(80) NOT NULL,
  `language` varchar(8) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_qbi_site_contact_info_qbi_site` (`site_id`),
  CONSTRAINT `FK_qbi_site_contact_info_qbi_site` FOREIGN KEY (`site_id`) REFERENCES `qbi_site` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for πίνακας qbi.qbi_site_images
CREATE TABLE IF NOT EXISTS `qbi_site_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `alt` text,
  `file` varchar(80) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__qbi_site` (`site_id`),
  CONSTRAINT `FK__qbi_site` FOREIGN KEY (`site_id`) REFERENCES `qbi_site` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for πίνακας qbi.qbi_site_sections
CREATE TABLE IF NOT EXISTS `qbi_site_sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `language` varchar(8) NOT NULL,
  `level` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKSite_ID` (`site_id`),
  CONSTRAINT `FKSite_ID` FOREIGN KEY (`site_id`) REFERENCES `qbi_site` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for πίνακας qbi.qbi_user
CREATE TABLE IF NOT EXISTS `qbi_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` text NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `iterations` int(1) NOT NULL,
  `type` int(1) NOT NULL,
  `domail` text,
  `created_at` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for πίνακας qbi.qbi_user_types
CREATE TABLE IF NOT EXISTS `qbi_user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
